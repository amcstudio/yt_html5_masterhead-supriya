"use strict";
~(function() {
  var $ = TweenMax,
    count = 2,
    loop = 0,
    transparentCta = document.getElementById('transparentCta'),
    bgExit = document.getElementById("bgExit"),
    cta = document.getElementById("cta");

  window.init = function() {
    $.set('.cta', { clearProps: 'all' })
    removeListeners();

    transparentCta.addEventListener('click', bgExitHandler)
    bgExit.addEventListener("click", bgExitHandler);
    var tl = new TimelineMax({});

    tl.set(".right", { x: 970, opacity: 1 });
    tl.set(".left", { x: -970, opacity: 1 });

    tl.to("#blackBg", 0.5, { y: 0, ease: Sine.easeInOut }, "+=3");
    tl.to( "#copyTwo", 0.5, { x: 0, ease: Sine.easeInOut });
    tl.to("#blackBg", 0.5, { backgroundColor:"#e1140a",opacity: 1, ease: Sine.easeInOut }, "+=1.5");
    tl.to("#copyThree", 0.5, { x:0, ease: Sine.easeInOut }, "-=.5");
    tl.to("#redlogo", 0.5, { opacity: 0, ease: Sine.easeInOut }, "-=.5");

    tl.to( "#bg2", 0.5, { x: 0, ease: Sine.easeInOut},"+=3")
    tl.to("#redlogo", 0.5, { opacity: 1, ease: Sine.easeInOut }, "-=.5");
    tl.to( "#copyFour", 0.5, { x: 0, ease: Sine.easeInOut},"-=.2")
    tl.to("#ctaContainer", 0.5, { x: 0, ease: Sine.easeInOut, onComplete: addListeners},"-=.5")
    tl.add(doLoop,"+=1.5")
    
  };

  function bgExitHandler(e) {
    window.open(window.clickTag);
  }
  function mouseOverAnim() {
    var tl = new TimelineMax();

    tl.to("#cta", 0.25, { opacity: 0, ease: Power1.easeOut });
  }
  function mouseOutAnim() {
    var tl = new TimelineMax();
    tl.to("#cta", 0.25, { opacity: 1,  ease: Power1.easeOut });
  }

  function addListeners() {
    transparentCta.addEventListener("mouseover", mouseOverAnim);
  
    transparentCta.addEventListener("mouseout", mouseOutAnim);
  }

  function removeListeners() {
    transparentCta.removeEventListener("mouseover", mouseOverAnim);

    transparentCta.removeEventListener("mouseout", mouseOutAnim);
  }
  function doLoop() {
    if (loop < count) {
      loop++;
      var tlLoop = new TimelineMax({});

      tlLoop.to(["#copyFour", "#ctaContainer","#bg2"], 0.5, { opacity: 0 }, "+=2");

      tlLoop.set(
        ["#blackBg", "#copyTwo", "#screen", "#copyThree", "#lenovoLogo"],
        { clearProps: "all" },
        0
      );
      tlLoop.add(function() {
        init();
      }, 3);
    }
  }
})();

"use strict";
~(function() {
  var $ = TweenMax,
   tl,
    ad = document.getElementById("mainContent"),
    bgExit = document.getElementById("bgExit"),
    close = document.getElementById("closeButton"),
    transparentCta = document.getElementById('transparentCta');
    bgExit.addEventListener('click', bgExitHandler, false);

  window.init = function() {
    $.set('.cta', { clearProps: 'all' })
    removeListeners();
    addListeners();

   
    bgExit.addEventListener("click", exitClickHandler);
    tl = new TimelineMax({});

    tl.set(".right", { x: 970, opacity: 1 });
    tl.set(".left", { x: -970, opacity: 1 });

    tl.to("#blackBg", 0.5, { y: 0, ease: Sine.easeInOut }, "+=3");
    tl.to( "#copyTwo", 0.5, { x: 0, ease: Sine.easeInOut });
    tl.to("#blackBg", 0.5, { backgroundColor:"#e1140a",opacity: 1, ease: Sine.easeInOut }, "+=1.5");
    tl.to("#copyThree", 0.5, { x:0, ease: Sine.easeInOut }, "-=.5");
    tl.to("#redlogo", 0.5, { opacity: 0, ease: Sine.easeInOut }, "-=.5");

    tl.to( "#bg2", 0.5, { x: 0, ease: Sine.easeInOut},"+=3")
    tl.to("#redlogo", 0.5, { opacity: 1, ease: Sine.easeInOut }, "-=.5");
    tl.to( "#copyFour", 0.5, { x: 0, ease: Sine.easeInOut},"-=.2")
    tl.to("#ctaContainer", 0.5, { x: 0, ease: Sine.easeInOut, onComplete: addListeners},"-=.5")  
  };

 
  function mouseOverAnim() {
    var tl = new TimelineMax();

    tl.to("#cta", 0.25, { opacity: 0, ease: Power1.easeOut });
  }
  function mouseOutAnim() {
    var tl = new TimelineMax();
    tl.to("#cta", 0.25, { opacity: 1,  ease: Power1.easeOut });
  }

  function addListeners() {
    transparentCta.addEventListener("mouseover", mouseOverAnim);
    transparentCta.addEventListener("mouseout", mouseOutAnim);
    bgExit.addEventListener('click', exitClickHandler);
    close.addEventListener('click', closeAd);
  }

  function removeListeners() {
    transparentCta.removeEventListener("mouseover", mouseOverAnim);

    transparentCta.removeEventListener("mouseout", mouseOutAnim);
  }

  function exitClickHandler() {
    tl.pause();

    var tll = new TimelineMax({});
    tll.set("#bg2", { x: 0 });
    tll.set(["#copyFour", "#ctaContainer"], { x: 0 });
    tll.set("#redLogo", { opacity: 1});
    tll.set(["#blackBg","#copyTwo","#copyThree","#redlogo"], {opacity: 0});

    Enabler.exit('Background Exit');

}

function closeAd() {
    ad.style.display = "none";

}

function bgExitHandler() {
  Enabler.exit('Background Exit');
  }

})();



window.onload = function() { 
    if (Enabler.isInitialized()) { 
        pageLoadedHandler(); 
    } else { 
        Enabler.addEventListener(studio.events.StudioEvent.INIT, pageLoadedHandler); 
    } 
  }; 
   
  function pageLoadedHandler() { 
    if (Enabler.isVisible()) { 
      init(); 
    } else { 
      Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, init); 
    } 
  }